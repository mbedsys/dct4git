# Deep clean tool for git

This tool is able to clean a GIT repository history from file using the following parameters:
 - ```-b,--protect-branches-pattern```: Don't touch to the file in the set of branch using a regular expression pattern
 - ```-t,--protect-tags-pattern```: Don't touch to the file in the set of tag using a regular expression pattern
 - ```-s,--selector```: Write a selector (expression) of file to clean (with file path and or size information) under gawk condition format. The fields ($1, $2 and $3) contain respectively the blob hash, the file size and the file path

This tool produce a SHA1 bolb list given to the bfg tool (https://rtyley.github.io/bfg-repo-cleaner/) using ```java -jar bfg-[...].jar -bi blog_list.txt```

Example of usage:
```
/dct4git.sh \
    -b '^(feature_1|wip_.*)$' \
    -t '^v2\.[0-9]+' \
    -s 'match($3, /^obsolete\//) || ($2 > (64*1024) && ! match($3, /\.(c|cc|cxx|cpp|h|hh|hxx|hpp|java|py)$/))' -n
```
